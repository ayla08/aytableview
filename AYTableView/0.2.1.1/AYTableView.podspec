#
# Be sure to run `pod lib lint AYTableView.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'AYTableView'
  s.version          = '0.2.1.1'
  s.summary          = 'AYTableView is meant to use uitableview in an easy way'
  s.swift_version    = '4.2'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      =  'AYTableView is meant to use uitableview in an easy way. it propose a specific way to do it.'

  s.homepage         = 'https://github.com/ayla08/AYTableView'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Khadija BENLAHMR' => 'khadija.benla@gmail.com' }
  s.source           = { :git => 'https://github.com/ayla08/AYTableView.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'AYTableView/Classes/**/*'
  
  # s.resource_bundles = {
  #   'AYTableView' => ['AYTableView/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
