//
//  DMCell.swift
//  DressMe
//
//  Created by Khadija benlahmr on 07/08/2018.
//  Copyright © 2018 Khadija benlahmr. All rights reserved.
//

import UIKit

public typealias AYCellProtocols = AYCellDefaultProtocol & AYCellVMProtocol

public protocol AYCellDefaultProtocol {
    var didSelectCell: () -> Void { get }
    var actions: [UITableViewRowAction] { get }
}

public extension AYCellDefaultProtocol {
    var didSelectCell: () -> Void { get { return {} } }
    var actions: [UITableViewRowAction] { get { return [] }}
}

//MARK: Protocol for viewModel in the cell

public protocol AYCellVMProtocol: AYCellDefaultProtocol {
    associatedtype ViewModelType
    var viewModel: ViewModelType? { get set }
}

//MARK: CellDefinition

open class AYCellDefinition {
    var didSelectCell: () -> Void = {}
    var cell: UITableViewCell = UITableViewCell()
    var actions: [UITableViewRowAction] = []
}

//The generic object to create cells
open class AYCell<T: AYTableViewCell & AYCellProtocols>: AYCellDefinition {
    
    fileprivate var viewModel: T.ViewModelType?

    public init(viewModel: T.ViewModelType? = nil,tableview: UITableView) {
        super.init()
        self.didSelectCell = didSelectCell
        self.viewModel = viewModel
        self.cell = cellTableView(tableView: tableview)
        self.didSelectCell = ((viewModel as? AYCellDefaultProtocol)?.didSelectCell) ?? {}
    }
    
    func cellTableView(tableView: UITableView) -> UITableViewCell {
        var cell: T? = tableView.dequeueReusableCell(withIdentifier: T.identifier) as? T
        if cell == nil {
            tableView.register(UINib(nibName: T.identifier, bundle: nil), forCellReuseIdentifier: T.identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: T.identifier) as? T
        }
        cell?.viewModel = viewModel
        return cell!
    }
    
}
