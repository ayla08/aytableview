//
//  AYTableViewCell.swift
//  DressMe
//
//  Created by Khadija benlahmr on 12/08/2018.
//  Copyright © 2018 Khadija benlahmr. All rights reserved.
//

import UIKit


open class AYTableViewCell: UITableViewCell {

    static var identifier: String {
        get {
            return String(describing: self)
        }
    }

}
