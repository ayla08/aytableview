//
//  AYSectionDefinition.swift
//  DressMe
//
//  Created by Khadija benlahmr on 12/08/2018.
//  Copyright © 2018 Khadija benlahmr. All rights reserved.
//

import Foundation

public struct AYSectionDefinition {
    var cellDefinitions: [AYCellDefinition]
    
    public init(cellDefinitions: [AYCellDefinition]) {
        self.cellDefinitions = cellDefinitions
    }
}
