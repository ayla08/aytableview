//
//  AYObservable.swift
//  AYTableView
//
//  Created by Khadija benlahmr on 06/01/2019.
//

open class AYObservable<T> {
    
    public typealias AYObserverBlock = (T) -> Void
    
    //MARK: -properties
    
    var observer: AYObserverBlock? = nil
    
    //MARK: -Life cycle
    
    public init(value: T) {
        self.value = value
    }
    
    public var value: T {
        didSet {
            self.observer?(value)
        }
    }
    
    //MARK: - Public API
    
    static public func >> (observable: AYObservable<T>, observableBlock: @escaping AYObserverBlock) {
        observable.observer = observableBlock
        observableBlock(observable.value)
    }
    
}

