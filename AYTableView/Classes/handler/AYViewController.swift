//
//  AYViewController.swift
//  DressMe
//
//  Created by Khadija benlahmr on 01/12/2018.
//  Copyright © 2018 Khadija benlahmr. All rights reserved.
//

import UIKit


open class AYViewController: UIViewController,  AYTableViewDataHandlerProtocol {
    
    @IBOutlet open weak var tableview: UITableView!
    let handler: AYTableViewHandler = AYTableViewHandler()
    
    open func sectionDefinition() -> [AYSectionDefinition] {
        return []
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        self.tableview.delegate = handler
        self.tableview.dataSource = handler
        handler.dataSource = self
        handler.reloadDefinitions()
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        handler.reloadDefinitions()
        self.tableview.reloadData()
    }
}
