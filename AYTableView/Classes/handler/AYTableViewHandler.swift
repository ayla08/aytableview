//
//  AYTableViewHandler.swift
//  DressMe
//
//  Created by Khadija benlahmr on 01/12/2018.
//  Copyright © 2018 Khadija benlahmr. All rights reserved.
//

import Foundation
import UIKit

public protocol AYTableViewDataHandlerProtocol {
    func sectionDefinition() -> [AYSectionDefinition]
}

open class AYTableViewHandler: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    var sectionDefinitions: [AYSectionDefinition] = []
    
    public var dataSource: AYTableViewDataHandlerProtocol?
    
    public func reloadDefinitions(){
        sectionDefinitions = dataSource?.sectionDefinition() ?? []
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return sectionDefinitions.count
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  sectionDefinitions[section].cellDefinitions.count
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if sectionDefinitions.count >= indexPath.section && sectionDefinitions[indexPath.section].cellDefinitions.count >= indexPath.row {
            return sectionDefinitions[indexPath.section].cellDefinitions[indexPath.row].cell
        } else {
            assert(false, "oups  cellrowat problem with index")
        }
        return UITableViewCell()
    }
    
    public func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        sectionDefinitions[indexPath.section].cellDefinitions[indexPath.row].didSelectCell()
    }
    
    public func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
       return sectionDefinitions[indexPath.section].cellDefinitions[indexPath.row].actions
    }
    
}
