//
//  cellTest.swift
//  DressMe
//
//  Created by Khadija benlahmr on 08/08/2018.
//  Copyright © 2018 Khadija benlahmr. All rights reserved.
//

import UIKit
import AYTableView

protocol viewModelCellDefault: AYCellDefaultProtocol {
    var title: String { get }
}

struct CellTestVM: viewModelCellDefault {
    let title: String
    var didSelectCell: () -> Void = {}
    var actions: [UITableViewRowAction] = []
    init(title: String) {
        self.title = title
    }
    
}

class CellTest: AYTableViewCell, AYCellVMProtocol {

    @IBOutlet weak var titleLabel: UILabel!
    var viewModel: viewModelCellDefault? {
        didSet {
            self.titleLabel.text = viewModel?.title
//            self.titleLabel.text = "test"

        }
    }
}
