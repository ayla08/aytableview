//
//  ViewController.swift
//  AYTableView
//
//  Created by Khadija BENLAHMR on 12/01/2018.
//  Copyright (c) 2018 Khadija BENLAHMR. All rights reserved.
//

import UIKit
import AYTableView

class ViewController: UIViewController, AYTableViewDataHandlerProtocol  {
    
    @IBOutlet weak var tableview: UITableView!
    let handler: AYTableViewHandler = AYTableViewHandler()
    let test: AYObservable<String> = AYObservable<String>(value:"test")
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sectionDefinition() -> [AYSectionDefinition] {
        var result = [AYCellDefinition]()
        
        for element in ["hello", "tot"] {
            var vm = CellTestVM(title: element)
            vm.didSelectCell = {
                print("did select \(element)")
            }
            
            let cell =  AYCell<CellTest>(viewModel: vm, tableview: tableview)
            result.append(cell)
        }
        
        var vmSwipe = CellTestVM(title:"swipe here")
        let more = UITableViewRowAction(style: .default, title: "More") { action, index in
            print("more button tapped")
        }
        more.backgroundColor = UIColor.blue
        
        
        vmSwipe.actions = [more]
        let cellvmSwipe =  AYCell<CellTest>(viewModel: vmSwipe, tableview: tableview)
        result.append(cellvmSwipe)
        
        return [AYSectionDefinition(cellDefinitions: result)]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableview.delegate = handler
        self.tableview.dataSource = handler
        handler.dataSource = self
        handler.reloadDefinitions()
    }
    
}

