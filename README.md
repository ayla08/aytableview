# AYTableView

This pod is destinated to use tableview with MVVM pattern easly

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

AYTableView is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'AYTableView'
```

## Author

Khadija BENLAHMR, khadija.benla@gmail.com

## License

AYTableView is available under the MIT license. See the LICENSE file for more info.
