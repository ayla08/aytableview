#
# Be sure to run `pod lib lint AYTableView.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'AYTableView'
  s.version          = '0.3.4.3'
  s.summary          = 'AYTableView is meant to use uitableview in an easy way'
  s.swift_version    = '4.2'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      =  'AYTableView is meant to use uitableview in an easy way. it propose a specific way to do it.'

  s.homepage         = 'https://gitlab.com/ayla08/aytableview'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Khadija BENLAHMR' => 'khadija.benla@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/ayla08/aytableview.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'AYTableView/Classes/**/*'
  

end
